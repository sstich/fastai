import fastai.vision as vs
from fastai.metrics import error_rate

BATCH_SIZE = 64
SIZE=224

def create_data():
    path = vs.untar_data(vs.URLs.PETS)
    print(f"Created data at {path}")
    annotation_path = path / 'annotations'  # Not a fan of this overloading.
    image_path = path / 'images'
    
    image_filenames = vs.get_image_files(image_path)


    # What does this actually do?
    # Utility class. Best guess:
    # Creates two iterators over the training and
    # validation set to feed chunks of size batch_size.
    # Looks from the code that 20% is used for validation by default.
    # Scales, augments, and resizes too as preprocessing.
    re_pattern = r'/([^/]+)_\d+.jpg$' 
    data = vs.ImageDataBunch.from_name_re(
            image_path,
            image_filenames,
            re_pattern,
            ds_tfms=vs.get_transforms(),
            size=SIZE,
            bs=BATCH_SIZE
    ).normalize(vs.imagenet_stats)
    return data

def train_model(data):
    learn = vs.create_cnn(data, vs.models.resnet34, metrics=error_rate)
    learn.fit_one_cycle(4) # number of epochs. Cycle is training rate cycle.
    learn.save('stage-1')
    return learn

if __name__ == '__main__':
    data = create_data()
    learn = train_model(data)
